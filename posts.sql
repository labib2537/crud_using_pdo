-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 04, 2023 at 06:34 AM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud_pdo`
--

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int NOT NULL,
  `post_title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `post_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `post_title`, `description`, `image`, `post_at`) VALUES
(2, 'Farhan', 'This is the first name', '647c1a4398e1dphotographer-1867417_1280.jpg', '2023-06-04'),
(3, 'Labib', 'This is the last name', '647c19df8466bpexels-gantas-vaičiulėnas-5956910.jpg', '2023-06-03'),
(4, 'Sayed Farhan Labib', 'This is my full name', '647c19a32c458pexels-atahan-demir-4162270.jpg', '2023-06-04'),
(6, 'Dog', 'animal', '647c14eb08232pexels-jacob-colvin-1761279.jpg', '2023-06-04'),
(8, 'Jitu', 'Handsome guy!', '647c1d317cc32pexels-leon-ardho-1552106.jpg', '2023-06-04'),
(9, 'Burger', 'Food', '647c214bb433a[CITYPNG.COM]Three Patty Burger Fast Food HD PNG - 1093x1278.png', '2023-06-04'),
(10, 'Bangabondu', 'rafsan jaman khan ', '647c2cb4480b6WhatsApp Image 2023-06-04 at 12.11.25 PM.jpeg', '2023-06-04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
